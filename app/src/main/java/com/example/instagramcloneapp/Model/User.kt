package com.example.instagramcloneapp.Model

data class User (
    var bio: String = "",
    var email: String = "",
    var fullName: String = "",
    var profilePic: String = "",
    var uid: String = "",
    var username: String = ""
)