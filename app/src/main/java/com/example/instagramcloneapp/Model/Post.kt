package com.example.instagramcloneapp.Model

data class Post (
    var postDescription: String = "",
    var postId: String = "",
    val postPic: String = "",
    val publisher: String = ""
)