package com.example.instagramcloneapp.Model

data class Comment (
    var comment: String = "",
    var commentPublisher: String = ""
)