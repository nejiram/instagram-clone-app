package com.example.instagramcloneapp

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.renderscript.Sampler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.instagramcloneapp.Model.Comment
import com.example.instagramcloneapp.Model.Notification
import com.example.instagramcloneapp.Model.Post
import com.example.instagramcloneapp.Model.User
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_account_settings.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

class AccountSettingsActivity : AppCompatActivity() {

    private lateinit var currentUser: FirebaseUser
    private var checker = false
    private var myUrl = ""
    private var imageUri: Uri? = null
    private var storageProfilePictureRef: StorageReference? = null

    private var deleteAccount: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_settings)

        currentUser = FirebaseAuth.getInstance().currentUser!!
        storageProfilePictureRef = FirebaseStorage.getInstance().reference.child("Profile Pictures")

        getUserInfo()

        close_edit_account_btn.setOnClickListener{
            finish()
        }

        logout_btn.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, SignInActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

        change_profile_pic_text_view.setOnClickListener{
            checker = true
            CropImage.activity()
                .setAspectRatio(1,1)
                .start(this)
        }

        save_account_info_btn.setOnClickListener{
            when {
                checker -> updateImageAndUserInfo()
                else -> updateUserInfoOnly()
            }
        }

        delete_account_btn.setOnClickListener{
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Delete Account")
                .setMessage("Are you sure you want do Delete your Instagram Clone account?")
                .setCancelable(true).setPositiveButton("Yes") { _, _ ->
                    deleteAccount = true
                    val intent = Intent(this, SignInActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.putExtra("deleteAccount", true)
                    intent.putExtra("userId", currentUser.uid)
                    FirebaseAuth.getInstance().signOut()
                    startActivity(intent)
                    finish()
                }.setNegativeButton("No") { dialog, _ ->
                    dialog.dismiss()
                }
            val alert = builder.create()
            alert.show()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val result = CropImage.getActivityResult(data)
            imageUri = result.uri
            profile_pic_edit_account.setImageURI(imageUri)
        } else {
            Toast.makeText(this, "Error occurred, please try again!", Toast.LENGTH_LONG).show()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (deleteAccount) {
            currentUser.delete()
        }
    }

    private fun updateImageAndUserInfo() {

        val newFullName = edit_account_new_full_name.text.toString().toLowerCase()
        val newUsername = edit_account_new_username.text.toString().toLowerCase()
        val newBio = edit_account_new_bio.text.toString()

        when {
            imageUri == null -> Toast.makeText(this, "Please select an image first!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(newFullName) -> Toast.makeText(this, "Full name is required!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(newUsername) -> Toast.makeText(this, "Username is required!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(newBio) -> Toast.makeText(this, "Please write your bio!", Toast.LENGTH_LONG).show()
            else -> {
                edit_account_progress_bar.visibility = View.VISIBLE

                val fileRef = storageProfilePictureRef!!.child(currentUser.uid + ".jpg")
                var uploadTask: StorageTask<*>
                uploadTask = fileRef.putFile(imageUri!!)
                uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                    if (!task.isSuccessful) {
                        task.exception.let {
                            throw it!!
                            edit_account_progress_bar.visibility = View.GONE
                        }
                    }
                    return@Continuation fileRef.downloadUrl
                }).addOnCompleteListener(OnCompleteListener<Uri> {task ->
                    if (task.isSuccessful) {
                        val downloadUrl = task.result
                        myUrl = downloadUrl.toString()

                        val usersRef = FirebaseDatabase.getInstance().reference.child("Users")

                        val userMap = HashMap<String, Any> ()

                        userMap["profilePic"] = myUrl
                        userMap["fullName"] = newFullName
                        userMap["username"] = newUsername
                        userMap["bio"] = newBio

                        usersRef.child(currentUser.uid).updateChildren(userMap)

                        Toast.makeText(this, "Changes saved", Toast.LENGTH_LONG).show()

                        val intent = Intent(this, MainActivity::class.java)
                        //intent.putExtra("AccountSettings", "ProfileFragment")
                        startActivity(intent)
                        finish()

                        edit_account_progress_bar.visibility = View.GONE

                    } else {
                        edit_account_progress_bar.visibility = View.GONE
                    }
                })
            }
        }
    }

    private fun updateUserInfoOnly() {

        val newFullName = edit_account_new_full_name.text.toString().toLowerCase()
        val newUsername = edit_account_new_username.text.toString().toLowerCase()
        val newBio = edit_account_new_bio.text.toString()

        when {
            TextUtils.isEmpty(newFullName) -> Toast.makeText(this, "Full name is required!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(newUsername) -> Toast.makeText(this, "Username is required!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(newBio) -> Toast.makeText(this, "Please write your bio!", Toast.LENGTH_LONG).show()
            else -> {
                edit_account_progress_bar.visibility = View.VISIBLE

                val usersRef = FirebaseDatabase.getInstance().reference.child("Users")

                val userMap = HashMap<String, Any> ()

                userMap["fullName"] = newFullName
                userMap["username"] = newUsername
                userMap["bio"] = newBio

                usersRef.child(currentUser.uid).updateChildren(userMap)

                Toast.makeText(this, "Changes saved", Toast.LENGTH_LONG).show()

                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()

                edit_account_progress_bar.visibility = View.GONE

            }
        }

    }

    private fun getUserInfo() {
        val usersRef = FirebaseDatabase.getInstance().reference.child("Users").child(currentUser.uid)
        usersRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val user = datasnapshot.getValue(User::class.java)
                    Picasso.get().load(user?.profilePic).placeholder(R.drawable.profile).into(profile_pic_edit_account)
                    edit_account_new_username.setText(user?.username)
                    edit_account_new_full_name.setText(user?.fullName)
                    edit_account_new_bio.setText(user?.bio)
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }


}