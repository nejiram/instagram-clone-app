package com.example.instagramcloneapp.Fragments

import android.os.Bundle
import android.renderscript.Sampler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Adapters.UserAdapter
import com.example.instagramcloneapp.Adapters.UserPicturesAdapter
import com.example.instagramcloneapp.Model.Post
import com.example.instagramcloneapp.Model.User
import com.example.instagramcloneapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import java.util.*
import kotlin.collections.ArrayList


class SearchFragment : Fragment() {

    private var currenteUser: FirebaseUser? = FirebaseAuth.getInstance().currentUser

    private var recyclerView: RecyclerView? = null
    private var userAdapter: UserAdapter? = null
    private var mUser: MutableList<User>? = null

    private var postList: MutableList<Post>? = null
    private var postsAdapter: UserPicturesAdapter? = null
    private var allPosts: MutableList<Post>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_search, container, false)

        recyclerView = view.findViewById(R.id.search_recyclerview)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(context)

        mUser = ArrayList()
        userAdapter = context?.let{ UserAdapter(it, mUser as java.util.ArrayList<User>, true) }
        recyclerView?.adapter = userAdapter

        val postRecyclerView: RecyclerView
        postRecyclerView = view.findViewById(R.id.search_pictures_recyclerview)
        postRecyclerView.setHasFixedSize(true)

        val linearLayoutManager: LinearLayoutManager = GridLayoutManager(context, 3)
        postRecyclerView.layoutManager = linearLayoutManager

        postList = ArrayList()
        postsAdapter = context?.let { UserPicturesAdapter(it, postList as ArrayList<Post>) }
        postRecyclerView.adapter = postsAdapter

        allPosts = ArrayList()

        recyclerView?.visibility = View.GONE
        postRecyclerView.visibility = View.VISIBLE

        view.search_editText.addTextChangedListener(object: TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (view.search_editText.text.toString() == "") {
                    recyclerView?.visibility = View.GONE
                    postRecyclerView.visibility = View.VISIBLE
                    getRandomPosts()

                } else {
                    postRecyclerView.visibility = View.GONE
                    recyclerView?.visibility = View.VISIBLE
                    retrieveUsers()
                    searchUser(s.toString().toLowerCase())
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        getAllPosts()
        getRandomPosts()

        return view
    }

    private fun searchUser(input: String) {
        val query = FirebaseDatabase.getInstance().reference.child("Users").orderByChild("fullName").startAt(input).endAt(input + "\uf8ff")
        query.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                mUser?.clear()
                for (snapshot in dataSnapshot.children) {
                    val user = snapshot.getValue(User::class.java)
                    if (user != null && user.uid != currenteUser?.uid) {
                        mUser?.add(user)
                    }
                }
                userAdapter?.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun retrieveUsers() {
        val usersRef = FirebaseDatabase.getInstance().reference.child("Users")
        usersRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (view?.search_editText?.text.toString() == "") {
                    mUser?.clear()
                    for (snapshot in datasnapshot.children) {
                        val user = snapshot.getValue(User::class.java)
                        if (user != null && user.uid != currenteUser?.uid) {
                            mUser?.add(user)
                        }
                    }
                    userAdapter?.notifyDataSetChanged()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun getRandomPosts() {
        postList!!.clear()
        for (i in allPosts!!) {
            val rand = Random().nextInt(allPosts!!.size)
            val post = allPosts!![rand]
            postList!!.add(post)
        }
        postsAdapter!!.notifyDataSetChanged()

    }

    private fun getAllPosts()  {
        val postsRef = FirebaseDatabase.getInstance().reference.child("Posts")
        postsRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    allPosts!!.clear()
                    for (snapshot in datasnapshot.children) {
                        val post = snapshot.getValue(Post::class.java)
                        if (post!!.publisher != currenteUser!!.uid) {
                            allPosts!!.add(post)
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

}