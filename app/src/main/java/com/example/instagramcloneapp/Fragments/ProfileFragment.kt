package com.example.instagramcloneapp.Fragments

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.AccountSettingsActivity
import com.example.instagramcloneapp.Adapters.UserPicturesAdapter
import com.example.instagramcloneapp.Model.Post
import com.example.instagramcloneapp.Model.User
import com.example.instagramcloneapp.R
import com.example.instagramcloneapp.ShowUsersActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlin.collections.ArrayList

class ProfileFragment : Fragment() {

    private var profileId: String? = null
    private var currentUser: FirebaseUser? = null

    private var postList: MutableList<Post>? = null
    private var userPicturesAdapter: UserPicturesAdapter? = null

    private var savedPostsList: MutableList<Post>? = null
    private var usersSavedPictures: MutableList<String>? = null
    private var savedPicturesAdapter: UserPicturesAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)

        currentUser = FirebaseAuth.getInstance().currentUser!!

        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)

        if (pref != null) {
            this.profileId = pref.getString("profileId", "none")!!
        }

        if (currentUser!!.uid == profileId) {
            // user is on his/her own profile
            view.edit_account_btn.text = getString(R.string.profile_fragment_edit_profile)
            view.profile_fragment_saved_images_btn.visibility = View.VISIBLE
        } else if (currentUser!!.uid != profileId) {
            // user is visiting someones profile
            checkFollowAndFollowing()
            view.profile_fragment_saved_images_btn.visibility = View.GONE
        }

        // user pictures recycler view
        val userPicturesRecyclerView: RecyclerView?
        userPicturesRecyclerView = view.findViewById(R.id.users_pictures_recyclerview)
        userPicturesRecyclerView.setHasFixedSize(true)

        val linearLayoutManager: LinearLayoutManager = GridLayoutManager(context, 3)
        userPicturesRecyclerView.layoutManager = linearLayoutManager

        postList = ArrayList()
        userPicturesAdapter = context?.let { UserPicturesAdapter(it, postList as ArrayList<Post>) }
        userPicturesRecyclerView.adapter = userPicturesAdapter

        // saved pictures recycler view
        val savedPicturesRecyclerView: RecyclerView?
        savedPicturesRecyclerView = view.findViewById(R.id.saved_pictures_recyclerview)
        savedPicturesRecyclerView.setHasFixedSize(true)

        val linearLayoutManagerSaved: LinearLayoutManager = GridLayoutManager(context, 3)
        savedPicturesRecyclerView.layoutManager = linearLayoutManagerSaved

        savedPostsList = ArrayList()
        savedPicturesAdapter = context?.let { UserPicturesAdapter(it, savedPostsList as ArrayList<Post>) }
        savedPicturesRecyclerView.adapter = savedPicturesAdapter

        savedPicturesRecyclerView.visibility = View.GONE
        userPicturesRecyclerView.visibility = View.VISIBLE

        view.profile_fragment_images_grid_view_btn.setOnClickListener{
            savedPicturesRecyclerView.visibility = View.GONE
            userPicturesRecyclerView.visibility = View.VISIBLE
        }

        view.profile_fragment_saved_images_btn.setOnClickListener{
            userPicturesRecyclerView.visibility = View.GONE
            savedPicturesRecyclerView.visibility = View.VISIBLE
        }

        view.edit_account_btn.setOnClickListener{

            when (view.edit_account_btn.text.toString()) {
                getString(R.string.profile_fragment_edit_profile) -> {
                    startActivity(Intent(context, AccountSettingsActivity::class.java))
                }
                getString(R.string.follow_button) -> {
                    currentUser!!.uid.let { it1 ->
                        FirebaseDatabase.getInstance().reference
                            .child("Follow").child(it1)
                            .child("Following").child(profileId!!)
                            .setValue(true)
                    }
                    currentUser!!.uid.let { it1 ->
                        FirebaseDatabase.getInstance().reference
                            .child("Follow").child(profileId!!)
                            .child("Followers").child(it1)
                            .setValue(true)
                    }
                    sendNotification()
                }
                getString(R.string.following_button) -> {
                    currentUser!!.uid.let { it1 ->
                        FirebaseDatabase.getInstance().reference
                            .child("Follow").child(it1)
                            .child("Following").child(profileId!!)
                            .removeValue()
                    }
                    currentUser!!.uid.let { it1 ->
                        FirebaseDatabase.getInstance().reference
                            .child("Follow").child(profileId!!)
                            .child("Followers").child(it1)
                            .removeValue()
                    }
                }
            }
        }

        view.followers_profile.setOnClickListener{
            val intent = Intent(context, ShowUsersActivity::class.java)
            intent.putExtra("id", profileId)
            intent.putExtra("title", "Followers")
            startActivity(intent)
        }

        view.following_profile.setOnClickListener{
            val intent = Intent(context, ShowUsersActivity::class.java)
            intent.putExtra("id", profileId)
            intent.putExtra("title", "Following")
            startActivity(intent)
        }

        getPosts()
        getFollowers()
        getFollowings()
        getUserInfo()
        getUsersPosts()
        getSavedPosts()

        return view
    }

    private fun checkFollowAndFollowing() {
        val followingRef = currentUser!!.uid.let { it1 ->
            FirebaseDatabase.getInstance().reference
                .child("Follow").child(it1)
                .child("Following")
        }
        followingRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.child(profileId!!).exists()) {
                    view?.edit_account_btn?.text = getString(R.string.following_button)
                    view?.edit_account_btn?.setBackgroundResource(R.color.colorBlue)
                    view?.edit_account_btn?.setTextColor(Color.WHITE)
                } else {
                    view?.edit_account_btn?.text = getString(R.string.follow_button)
                    view?.edit_account_btn?.setBackgroundResource(R.color.colorBlue)
                    view?.edit_account_btn?.setTextColor(Color.WHITE)
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun getPosts() {
        val postsRef = FirebaseDatabase.getInstance().reference.child("Posts")

        postsRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    var postCounter = 0
                    for (snapshot in datasnapshot.children) {
                        val post = snapshot.getValue(Post::class.java)
                        if (post!!.publisher == profileId) {
                            postCounter++
                        }
                    }
                    view?.total_posts!!.text = postCounter.toString()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }

    private fun getFollowers() {
        val followersRef = FirebaseDatabase.getInstance().reference
                .child("Follow").child(profileId!!)
                .child("Followers")

        followersRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    view?.total_followers?.text = datasnapshot.childrenCount.toString()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun getFollowings() {
        val followingRef = FirebaseDatabase.getInstance().reference
                .child("Follow").child(profileId!!)
                .child("Following")

        followingRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    view?.total_following?.text = (datasnapshot.childrenCount - 1).toString()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun getUserInfo() {
        val usersRef = FirebaseDatabase.getInstance().reference.child("Users").child(profileId!!)
        usersRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val user = datasnapshot.getValue(User::class.java)
                    Picasso.get().load(user?.profilePic).placeholder(R.drawable.profile).into(view!!.profile_fragment_profile_image)
                    view?.profile_fragment_username?.text = user?.username
                    view?.profile_fragment_full_name?.text = user?.fullName
                    view?.profile_fragment_bio?.text = user?.bio
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun getUsersPosts() {
        val postsRef = FirebaseDatabase.getInstance().reference.child("Posts")

        postsRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    postList!!.clear()

                    for (snapshot in datasnapshot.children) {
                        val post = snapshot.getValue(Post::class.java)
                        if (post!!.publisher == profileId) {
                            postList!!.add(post)
                        }
                    }
                    postList!!.reverse()
                    userPicturesAdapter!!.notifyDataSetChanged()

                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }

    private fun getSavedPosts() {
        usersSavedPictures = ArrayList()
        val savesRef = FirebaseDatabase.getInstance().reference
            .child("Saves").child(currentUser!!.uid)

        savesRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        (usersSavedPictures as ArrayList<String>).add(snapshot.key.toString())
                    }
                    readSavedPosts()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun readSavedPosts() {

        val postsRef = FirebaseDatabase.getInstance().reference.child("Posts")

        postsRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    savedPostsList!!.clear()
                    for (snapshot in datasnapshot.children) {
                        val post = snapshot.getValue(Post::class.java)

                        for (key in usersSavedPictures!!) {
                            if (post!!.postId == key) {
                                savedPostsList!!.add(post)
                            }
                        }
                    }
                    savedPicturesAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }

    override fun onStop() {
        super.onStop()

        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId", currentUser!!.uid)
        pref?.apply()

    }

    override fun onPause() {
        super.onPause()

        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId", currentUser!!.uid)
        pref?.apply()
    }

    override fun onDestroy() {
        super.onDestroy()

        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId", currentUser!!.uid)
        pref?.apply()
    }

    private fun sendNotification() {
        val notificationsRef = FirebaseDatabase.getInstance().reference
            .child("Notifications").child(profileId!!)

        val notificationMap = HashMap<String, Any>()
        notificationMap["userId"] = this.currentUser!!.uid
        notificationMap["text"] = "started following you."
        notificationMap["postId"] = ""
        notificationMap["isPost"] = false

        notificationsRef.push().setValue(notificationMap)
    }

}