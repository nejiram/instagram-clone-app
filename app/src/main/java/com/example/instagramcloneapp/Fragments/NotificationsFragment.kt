package com.example.instagramcloneapp.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Adapters.NotificationAdapter
import com.example.instagramcloneapp.Model.Notification
import com.example.instagramcloneapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*
import kotlin.collections.ArrayList

class NotificationsFragment : Fragment() {

    private var currentUser: FirebaseUser? = null

    private var notificationsList: MutableList<Notification> ?= null
    private var notificationAdapter: NotificationAdapter ?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_notifications, container, false)

        currentUser = FirebaseAuth.getInstance().currentUser

        val recyclerView: RecyclerView?
        recyclerView = view.findViewById(R.id.recyclerview_notifications)

        recyclerView.layoutManager = LinearLayoutManager(context)

        notificationsList = ArrayList()
        notificationAdapter = context?.let { NotificationAdapter(it, notificationsList as ArrayList<Notification>) }
        recyclerView.adapter = notificationAdapter

        readNotifications()

        return view
    }

    private fun readNotifications() {
        val notificationsRef = FirebaseDatabase.getInstance().reference
            .child("Notifications").child(currentUser!!.uid)
        notificationsRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    (notificationsList as ArrayList<Notification>).clear()
                    for (snapshot in datasnapshot.children) {
                        val notification = snapshot.getValue(Notification::class.java)
                        if (notification!!.userId != currentUser!!.uid) {
                            (notificationsList as ArrayList<Notification>).add(notification!!)
                        }
                    }
                    (notificationsList as ArrayList<Notification>).reverse()
                    notificationAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

}