package com.example.instagramcloneapp.Fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Adapters.PostAdapter
import com.example.instagramcloneapp.MainActivity
import com.example.instagramcloneapp.Model.Post
import com.example.instagramcloneapp.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_post_details.*
import kotlinx.android.synthetic.main.fragment_post_details.view.*


class PostDetailsFragment : Fragment() {

    private var postList: MutableList<Post>? = null
    private var postDetailsAdapter: PostAdapter? = null
    private var postId: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_post_details, container, false)

        val preferences = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)
        if (preferences != null) {
            postId = preferences.getString("postId", "non").toString()
        }

        var postDetailsRecyclerView: RecyclerView? = null
        postDetailsRecyclerView = view.findViewById(R.id.photo_details_recyclerview)
        postDetailsRecyclerView.setHasFixedSize(true)

        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true
        postDetailsRecyclerView.layoutManager = linearLayoutManager

        postList = ArrayList()
        postDetailsAdapter = context?.let { PostAdapter(it, postList as ArrayList<Post>) }
        postDetailsRecyclerView.adapter = postDetailsAdapter

        view.post_back_btn.setOnClickListener{
            val intent = Intent(context, MainActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }

        retrievePost()

        return view

    }

    private fun retrievePost() {
        val postsRef = FirebaseDatabase.getInstance().reference.child("Posts").child(postId)
        postsRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                postList?.clear()
                if (datasnapshot.exists()) {
                    val post = datasnapshot.getValue(Post::class.java)
                    postList!!.add(post!!)
                    postDetailsAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }


}