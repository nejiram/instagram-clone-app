package com.example.instagramcloneapp

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_account_settings.*
import kotlinx.android.synthetic.main.activity_add_new_post.*

class AddNewPostActivity : AppCompatActivity() {

    private var myUrl = ""
    private var imageUri: Uri? = null
    private var storagePostRef: StorageReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_post)

        storagePostRef = FirebaseStorage.getInstance().reference.child("Posts")

        close_add_new_post_btn.setOnClickListener{
            finish()
        }

        add_new_post_btn.setOnClickListener{
            uploadPost()
        }

        CropImage.activity()
            .setAspectRatio(1,1)
            .start(this)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val result = CropImage.getActivityResult(data)
            imageUri = result.uri
            new_image_post.setImageURI(imageUri)
        } else {
            Toast.makeText(this, "Error occurred, please try again!", Toast.LENGTH_LONG).show()
        }

    }

    private fun uploadPost() {

        val newPostDescription = new_post_description.text.toString()

        when {
            imageUri == null -> Toast.makeText(this, "Please select an image first!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(newPostDescription) -> Toast.makeText(this, "Please write a caption!", Toast.LENGTH_LONG).show()
            else -> {
                add_new_post_progress_bar.visibility = View.VISIBLE

                val fileRef = storagePostRef!!.child(System.currentTimeMillis().toString() + ".jpg")
                val uploadTask: StorageTask<*>
                uploadTask = fileRef.putFile(imageUri!!)
                uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                    if (!task.isSuccessful) {
                        task.exception.let {
                            edit_account_progress_bar.visibility = View.GONE
                            throw it!!
                        }
                    }
                    return@Continuation fileRef.downloadUrl
                }).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUrl = task.result
                        myUrl = downloadUrl.toString()

                        val ref = FirebaseDatabase.getInstance().reference.child("Posts")

                        val postMap = HashMap<String, Any>()

                        val postId = ref.push().key

                        postMap["postId"] = postId!!
                        postMap["publisher"] = FirebaseAuth.getInstance().currentUser!!.uid
                        postMap["postPic"] = myUrl
                        postMap["postDescription"] = newPostDescription

                        ref.child(postId).updateChildren(postMap)

                        Toast.makeText(this, "Posted!", Toast.LENGTH_LONG).show()

                        finish()

                        add_new_post_progress_bar.visibility = View.GONE

                    } else {
                        add_new_post_progress_bar.visibility = View.GONE
                    }
                }

            }
        }
    }
}