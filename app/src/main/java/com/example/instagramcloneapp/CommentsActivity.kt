package com.example.instagramcloneapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Adapters.CommentAdapter
import com.example.instagramcloneapp.Model.Post
import com.example.instagramcloneapp.Model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_comments.*
import com.example.instagramcloneapp.Model.Comment
import com.google.firebase.ktx.Firebase

class CommentsActivity : AppCompatActivity() {

    private var postId: String? = null
    private var publisherId: String? = null

    private var currentUser: FirebaseUser? = null

    private var commentAdapter: CommentAdapter? = null
    private var commentList: MutableList<Comment>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)

        currentUser = FirebaseAuth.getInstance().currentUser

        var recyclerView: RecyclerView
        recyclerView = findViewById(R.id.comments_recyclerview)
        var linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        commentList = ArrayList()
        commentAdapter = CommentAdapter(this, commentList as ArrayList<Comment>)
        recyclerView.adapter = commentAdapter

        comments_back_btn.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        add_comment_btn.setOnClickListener{
            if (comment_text.text.toString() != "") {
                addComment()
            }
        }

        postId = intent.getStringExtra("postId")
        publisherId = intent.getStringExtra("publisherId")

        getPostsInfo()
        getPostsDescription()
        readComments()

    }

    private fun addComment() {

        val commentRef = FirebaseDatabase.getInstance().reference.child("Comments").child(postId!!)

        val commentMap = HashMap<String, Any> ()
        commentMap["comment"] = comment_text.text.toString()
        commentMap["commentPublisher"] = currentUser!!.uid

        commentRef.push().setValue(commentMap)

        sendNotification()

        comment_text.text.clear()

    }

    private fun getPostsInfo() {

        val publisherRef = FirebaseDatabase.getInstance().reference.child("Users").child(publisherId!!)

        publisherRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val publisher = datasnapshot.getValue(User::class.java)
                    Picasso.get().load(publisher!!.profilePic).resize(40, 40).into(comments_publisher_profile_picture)
                    comments_publisher_username.text = publisher.username
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }

    private fun getPostsDescription() {

        val postRef = FirebaseDatabase.getInstance().reference.child("Posts").child(postId!!)

        postRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val post = datasnapshot.getValue(Post::class.java)
                    comments_post_description.text = post!!.postDescription
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }

    private fun readComments() {

        val commentsRef = FirebaseDatabase.getInstance().reference.child("Comments").child(postId!!)

        commentsRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    commentList!!.clear()
                    for (snapshot in datasnapshot.children) {
                        val comment = snapshot.getValue(Comment::class.java)
                        commentList!!.add(comment!!)
                    }
                    commentAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }

    private fun sendNotification() {
        val notificationsRef = FirebaseDatabase.getInstance().reference
            .child("Notifications").child(publisherId!!)

        val notificationMap = HashMap<String, Any>()
        notificationMap["userId"] = currentUser!!.uid
        notificationMap["text"] = "left a comment on your post: \"${comment_text.text}\"."
        notificationMap["postId"] = postId!!
        notificationMap["isPost"] = true

        notificationsRef.push().setValue(notificationMap)

    }


}