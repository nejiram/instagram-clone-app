package com.example.instagramcloneapp

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.instagramcloneapp.Model.Comment
import com.example.instagramcloneapp.Model.Notification
import com.example.instagramcloneapp.Model.Post
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_account_settings.*
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        val userId = intent.getStringExtra("userId")
        val deleteAccount = intent.getBooleanExtra("deleteAccount", false)

        signup_link_btn.setOnClickListener{
            startActivity(Intent(this, SignUpActivity::class.java))
        }

        signin_btn.setOnClickListener{
            loginUser()
        }

        if (deleteAccount) {
            deleteUsersAccount(userId!!)
        }

    }

    private fun loginUser() {
        val email = signin_email.text.toString()
        val password = signin_password.text.toString()

        when {
            TextUtils.isEmpty(email) -> Toast.makeText(this, "Username is required!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(password) -> Toast.makeText(this, "Password is required!", Toast.LENGTH_LONG).show()
            else -> {

                sign_in_progress_bar.visibility = View.VISIBLE

                val mAuth: FirebaseAuth = FirebaseAuth.getInstance()

                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener{task ->
                    if (task.isSuccessful) {
                        sign_in_progress_bar.visibility = View.GONE
                        val intent = Intent(this, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        val message = task.exception.toString()
                        Toast.makeText(this, "Error: $message", Toast.LENGTH_LONG).show()
                        mAuth.signOut()
                        sign_in_progress_bar.visibility = View.GONE
                    }
                }

            }
        }

    }

    override fun onStart() {
        super.onStart()

        if (FirebaseAuth.getInstance().currentUser != null) {
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

    }

    private fun deleteUsersAccount(userId: String) {

        deleteFromUsers(userId)
        deleteFromSaves(userId)
        deleteFromPosts(userId)
        deleteFromNotifications(userId)
        deleteUsersLikes(userId)
        deleteUsersComments(userId)
        deleteFromFollow(userId)

    }

    private fun deleteFromUsers(userId: String) {
        FirebaseDatabase.getInstance().reference.child("Users").child(userId).removeValue()
    }

    private fun deleteFromSaves(userId: String) {
        FirebaseDatabase.getInstance().reference.child("Saves").child(userId).removeValue()
    }

    private fun deleteFromPosts(userId: String) {
        val postsRef = FirebaseDatabase.getInstance().reference.child("Posts")
        postsRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        val post = snapshot.getValue(Post::class.java)
                        if (post!!.publisher == userId) {
                            FirebaseDatabase.getInstance().reference.child("Posts").child(snapshot.key!!).removeValue()

                            FirebaseDatabase.getInstance().reference.child("Likes").child(post.postId).removeValue()
                            FirebaseDatabase.getInstance().reference.child("Comments").child(post.postId).removeValue()

                            deletePostSaves(post.postId)
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun deletePostSaves(postId: String) {
        val savesRef = FirebaseDatabase.getInstance().reference.child("Saves")
        savesRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        for (s in snapshot.children) {
                            if (s.key!! == postId) {
                                FirebaseDatabase.getInstance().reference.child("Posts")
                                    .child(snapshot.key!!).child(s.key!!).removeValue()
                            }
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun deleteFromNotifications(userId: String) {
        val notificationsRef = FirebaseDatabase.getInstance().reference.child("Notifications")
        notificationsRef.child(userId).removeValue()
        notificationsRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        for (s in snapshot.children) {
                            val notification = s.getValue(Notification::class.java)
                            if (notification!!.userId == userId) {
                                FirebaseDatabase.getInstance().reference.child("Notifications")
                                    .child(snapshot.key!!).child(s.key!!).removeValue()
                            }
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun deleteUsersLikes(userId: String) {
        val likesRef = FirebaseDatabase.getInstance().reference.child("Likes")
        likesRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        for (s in snapshot.children) {
                            if (s.key!! == userId) {
                                FirebaseDatabase.getInstance().reference.child("Likes")
                                    .child(snapshot.key!!).child(s.key!!).removeValue()
                            }
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun deleteUsersComments(userId: String) {
        val commentsRef = FirebaseDatabase.getInstance().reference.child("Comments")
        commentsRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        for (s in snapshot.children) {
                            val comment = s.getValue(Comment::class.java)
                            if (comment!!.commentPublisher == userId) {
                                FirebaseDatabase.getInstance().reference.child("Comments")
                                    .child(snapshot.key!!).child(s.key!!).removeValue()
                            }
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun deleteFromFollow(userId: String) {
        val followRef = FirebaseDatabase.getInstance().reference.child("Follow")
        followRef.child(userId).removeValue()
        followRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        deleteFollowers(userId, snapshot.key!!)
                        deleteFollowing(userId, snapshot.key!!)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun deleteFollowers(userId: String, key: String) {
        val followersRef = FirebaseDatabase.getInstance().reference
            .child("Follow").child(key).child("Followers")
        followersRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        if (snapshot.key!! == userId) {
                            FirebaseDatabase.getInstance().reference.child("Follow")
                                .child(key).child("Followers").child(snapshot.key!!).removeValue()
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun deleteFollowing(userId: String, key: String) {
        val followersRef = FirebaseDatabase.getInstance().reference
            .child("Follow").child(key).child("Following")
        followersRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    for (snapshot in datasnapshot.children) {
                        if (snapshot.key!! == userId) {
                            FirebaseDatabase.getInstance().reference.child("Follow")
                                .child(key).child("Following").child(snapshot.key!!).removeValue()
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }



}