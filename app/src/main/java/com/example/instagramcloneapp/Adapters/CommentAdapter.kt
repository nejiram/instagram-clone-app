package com.example.instagramcloneapp.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Model.Comment
import com.example.instagramcloneapp.Model.User
import com.example.instagramcloneapp.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class CommentAdapter(private val mContext: Context,
                     private val mComment: MutableList<Comment>): RecyclerView.Adapter<CommentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.comment_item_layout, parent, false)
        return CommentAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val comment = mComment[position]

        setPublisherInfo(holder.publisherProfilePicture, holder.publisherUsername, comment.commentPublisher)
        holder.commentText.text = comment.comment

    }

    override fun getItemCount(): Int {
        return mComment.size
    }

    class ViewHolder (@NonNull itemView: View): RecyclerView.ViewHolder(itemView) {
        val publisherProfilePicture: CircleImageView = itemView.findViewById(R.id.publisher_profile_picture_comment)
        val publisherUsername: TextView = itemView.findViewById(R.id.publisher_username_comment)
        val commentText: TextView = itemView.findViewById(R.id.comment)

    }

    private fun setPublisherInfo(profilePic: CircleImageView, username: TextView,  commentPublisher: String) {

        val publisherRef = FirebaseDatabase.getInstance().reference.child("Users").child(commentPublisher)

        publisherRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val publisher = datasnapshot.getValue(User::class.java)
                    Picasso.get().load(publisher!!.profilePic).resize(40,40).into(profilePic)
                    username.text = publisher.username
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }
}