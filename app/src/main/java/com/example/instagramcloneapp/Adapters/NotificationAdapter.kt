package com.example.instagramcloneapp.Adapters

import android.content.Context
import android.graphics.Typeface
import android.renderscript.Sampler
import android.text.*
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Fragments.PostDetailsFragment
import com.example.instagramcloneapp.Fragments.ProfileFragment
import com.example.instagramcloneapp.Model.Notification
import com.example.instagramcloneapp.Model.Post
import com.example.instagramcloneapp.Model.User
import com.example.instagramcloneapp.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.notifications_item_layout.view.*

class NotificationAdapter(private val mContext: Context,
                          private val mNotification: MutableList<Notification>) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.notifications_item_layout, parent, false)
        return NotificationAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val notification: Notification = mNotification[position]

        when {
            "following" in notification.text -> {
                holder.postPicture.visibility = View.GONE
            }
            "liked" in notification.text -> {
                holder.postPicture.visibility = View.VISIBLE
                setPostPicture(holder.postPicture, notification.postId)
            }
            "comment" in notification.text -> {
                holder.postPicture.visibility = View.VISIBLE
                setPostPicture(holder.postPicture, notification.postId)
            }
        }

        holder.notificationText.text = notification.text

        setUserInfo(holder.userProfilePic, holder.username, notification.userId)

        holder.username.setOnClickListener{
            val pref = mContext.getSharedPreferences("PREFS", Context.MODE_PRIVATE).edit()
            pref.putString("profileId", notification.userId)
            pref.apply()

            (mContext as FragmentActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, ProfileFragment()).commit()
        }

        holder.postPicture.setOnClickListener{
            val editor = mContext.getSharedPreferences("PREFS", Context.MODE_PRIVATE).edit()
            editor.putString("postId", notification.postId)
            editor.apply()
            (mContext as FragmentActivity).supportFragmentManager.beginTransaction().replace(R.id.fragment_container, PostDetailsFragment()).commit()
        }

    }

    override fun getItemCount(): Int {
        return mNotification.size
    }

    class ViewHolder (@NonNull itemView: View): RecyclerView.ViewHolder(itemView) {
        val userProfilePic: CircleImageView = itemView.findViewById(R.id.profile_picture_notifications)
        val username: TextView = itemView.findViewById(R.id.username_notifications)
        val notificationText: TextView = itemView.findViewById(R.id.notification_text)
        val postPicture: ImageView = itemView.findViewById(R.id.post_picture_notifications)
    }

    private fun setPostPicture(postPic: ImageView, postId: String) {
        val postRef = FirebaseDatabase.getInstance().reference.child("Posts").child(postId)
        postRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val post = datasnapshot.getValue(Post::class.java)
                    Picasso.get().load(post!!.postPic).resize(60,60).placeholder(R.drawable.profile).into(postPic)

                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }


    private fun setUserInfo(userProfilePic: CircleImageView, username: TextView, userId: String) {
        val userRef = FirebaseDatabase.getInstance().reference.child("Users").child(userId)
        userRef.addValueEventListener(object:ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val user = datasnapshot.getValue(User::class.java)
                    Picasso.get().load(user!!.profilePic).resize(40, 40).into(userProfilePic)
                    username.text = user.username
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

}