package com.example.instagramcloneapp.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Fragments.PostDetailsFragment
import com.example.instagramcloneapp.Model.Post
import com.example.instagramcloneapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.squareup.picasso.Picasso

class UserPicturesAdapter (private val mContext: Context, private  val mPost: List<Post>):
        RecyclerView.Adapter<UserPicturesAdapter.ViewHolder>() {

    private var currentUser: FirebaseUser?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserPicturesAdapter.ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.picture_item_layout, parent, false)

        return UserPicturesAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserPicturesAdapter.ViewHolder, position: Int) {

        currentUser = FirebaseAuth.getInstance().currentUser

        val post = mPost[position]

        Picasso.get().load(post.postPic).resize(125, 125).into(holder.postPicture)

        holder.postPicture.setOnClickListener{
            val editor = mContext.getSharedPreferences("PREFS", Context.MODE_PRIVATE).edit()
            editor.putString("postId", post.postId)
            editor.apply()
            (mContext as FragmentActivity).supportFragmentManager.beginTransaction().replace(R.id.fragment_container, PostDetailsFragment()).commit()
        }

    }

    override fun getItemCount(): Int {
        return mPost.size
    }

    class ViewHolder (@NonNull itemView: View): RecyclerView.ViewHolder(itemView) {

        val postPicture: ImageView = itemView.findViewById(R.id.post_picture_profile)

    }

}