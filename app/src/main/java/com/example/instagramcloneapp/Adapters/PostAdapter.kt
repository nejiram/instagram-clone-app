package com.example.instagramcloneapp.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.ddd.androidutils.DoubleClick
import com.ddd.androidutils.DoubleClickListener
import com.example.instagramcloneapp.CommentsActivity
import com.example.instagramcloneapp.Fragments.ProfileFragment
import com.example.instagramcloneapp.Model.Post
import com.example.instagramcloneapp.Model.User
import com.example.instagramcloneapp.R
import com.example.instagramcloneapp.ShowUsersActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_comments.*
import kotlinx.android.synthetic.main.posts_item_layout.view.*


class PostAdapter (private val mContext: Context,
                   private val mPost: List<Post>,
                   private val isFragment: Boolean = false): RecyclerView.Adapter<PostAdapter.ViewHolder>() {

    private var currentUser: FirebaseUser? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.posts_item_layout, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        currentUser = FirebaseAuth.getInstance().currentUser

        val post = mPost[position]
        Picasso.get().load(post.postPic).into(holder.postPic)

        if (post.postDescription.equals("")) {
            holder.postDescription.visibility = View.GONE
        } else {
            holder.postDescription.visibility = View.VISIBLE
            holder.postDescription.text = post.postDescription
        }

        publisherInfo(holder.profilePicture, holder.postsPublisherUsername, holder.postPublisher, post.publisher)

        isLiked(post.postId, holder.likeBtn)

        totalLikes(post.postId, holder.postsLikes)

        isSaved(post.postId, holder.savePostBtn)

        holder.likeBtn.setOnClickListener{
            likeOrUnlike(post, holder.likeBtn)
        }

        val doubleClick = DoubleClick(object: DoubleClickListener{
            override fun onSingleClickEvent(view: View?) {

            }

            override fun onDoubleClickEvent(view: View?) {
                likeOrUnlike(post, holder.likeBtn)
            }
        })

        holder.postPic.setOnClickListener(doubleClick)

        totalComments(post.postId, holder.postsComments)

        holder.commentBtn.setOnClickListener{
            addComment(post)
        }

        holder.postsComments.setOnClickListener{
            addComment(post)
        }

        holder.profilePicture.setOnClickListener{
            viewPublishersProfile(post.publisher)
        }

        holder.postsPublisherUsername.setOnClickListener{
            viewPublishersProfile(post.publisher)
        }

        holder.postPublisher.setOnClickListener{
            viewPublishersProfile(post.publisher)
        }

        holder.savePostBtn.setOnClickListener{

            when (holder.savePostBtn.tag) {
                "Save" -> {
                    FirebaseDatabase.getInstance().reference.child("Saves")
                        .child(currentUser!!.uid).child(post.postId).setValue(true)
                }
                "Saved" -> {
                    FirebaseDatabase.getInstance().reference.child("Saves")
                        .child(currentUser!!.uid).child(post.postId).removeValue()
                }
            }

        }

        holder.postsLikes.setOnClickListener{
            val intent = Intent(mContext, ShowUsersActivity::class.java)
            intent.putExtra("id", post.postId)
            intent.putExtra("title", "Likes")
            mContext.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return mPost.size
    }

    class ViewHolder (@NonNull itemView: View): RecyclerView.ViewHolder(itemView) {
        var profilePicture: CircleImageView = itemView.findViewById(R.id.posts_publisher_profile_picture)
        var postsPublisherUsername: TextView = itemView.findViewById(R.id.posts_publisher_username)
        var postPic: ImageView = itemView.findViewById(R.id.post_image_home)
        var likeBtn: ImageView = itemView.findViewById(R.id.like_post_btn)
        var commentBtn: ImageView = itemView.findViewById(R.id.comment_post_btn)
        var savePostBtn: ImageView = itemView.findViewById(R.id.save_post_btn)
        var postsLikes: TextView = itemView.findViewById(R.id.posts_likes)
        var postPublisher: TextView = itemView.findViewById(R.id.post_publisher)
        var postDescription: TextView = itemView.findViewById(R.id.post_description)
        var postsComments: TextView = itemView.findViewById(R.id.posts_comments)

    }

    private fun publisherInfo(profilePic: CircleImageView, postsPublisherUsername: TextView, postPublisher: TextView, publisherId: String) {

        val usersRef = FirebaseDatabase.getInstance().reference.child("Users").child(publisherId)

        usersRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val user = datasnapshot.getValue(User::class.java)
                    Picasso.get().load(user!!.profilePic).resize(50,50).placeholder(R.drawable.profile).into(profilePic)
                    postsPublisherUsername.text = user.username
                    postPublisher.text = user.username
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun isLiked(postId: String, likeBtn: ImageView) {

        val likesRef = FirebaseDatabase.getInstance().reference.child("Likes").child(postId)

        likesRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.child(currentUser!!.uid).exists()) {
                    likeBtn.setImageResource(R.drawable.heart_clicked)
                    likeBtn.tag = "Liked"
                } else {
                    likeBtn.setImageResource(R.drawable.heart_not_clicked)
                    likeBtn.tag = "Like"
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun totalLikes(postId: String, postsLikes: TextView) {

        val likesRef = FirebaseDatabase.getInstance().reference.child("Likes").child(postId)

        likesRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val numberOfLikes = datasnapshot.childrenCount
                    when {
                        numberOfLikes.toInt() == 1 -> postsLikes.text = numberOfLikes.toString() + " like"
                        else -> postsLikes.text = numberOfLikes.toString() + " likes"
                    }
                }
                else {
                    postsLikes.text = "0 likes"
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun totalComments(postId: String, postsComments: TextView) {

        val commentsRef = FirebaseDatabase.getInstance().reference.child("Comments").child(postId)

        commentsRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    val numberOfComments = datasnapshot.childrenCount.toString()
                    postsComments.text = "View all $numberOfComments comments"
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun addComment(p: Post) {
        val intentComments = Intent(mContext, CommentsActivity::class.java)
        intentComments.putExtra("postId", p.postId)
        intentComments.putExtra("publisherId", p.publisher)
        mContext.startActivity(intentComments)
    }

    private fun likeOrUnlike(post: Post, icon: ImageView) {
        when (icon.tag) {
            "Like" -> {
                // if user wants to like the post
                FirebaseDatabase.getInstance().reference.child("Likes")
                    .child(post.postId).child(currentUser!!.uid).setValue(true)
                sendNotification(post.publisher, post.postId)
            }
            "Liked" -> {
                // if user wants to unlike the post
                FirebaseDatabase.getInstance().reference.child("Likes")
                    .child(post.postId).child(currentUser!!.uid).removeValue()
            }
        }
    }

    private fun viewPublishersProfile(publisherId: String) {
        val pref = mContext.getSharedPreferences("PREFS", Context.MODE_PRIVATE).edit()
        pref.putString("profileId", publisherId)
        pref.apply()

        (mContext as FragmentActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, ProfileFragment()).commit()
    }

    private fun isSaved(postId: String, icon: ImageView) {

        val savesRef = FirebaseDatabase.getInstance().reference.child("Saves").child(currentUser!!.uid)

        savesRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.child(postId).exists()) {
                    icon.setImageResource(R.drawable.save_icon)
                    icon.tag = "Saved"
                } else {
                    icon.setImageResource(R.drawable.save_unfilled_icon)
                    icon.tag = "Save"
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }

    private fun sendNotification(userId: String, postId: String) {
        val notificationsRef = FirebaseDatabase.getInstance().reference
            .child("Notifications").child(userId)

        val notificationMap = HashMap<String, Any>()
        notificationMap["userId"] = currentUser!!.uid
        notificationMap["text"] = "liked your post."
        notificationMap["postId"] = postId
        notificationMap["isPost"] = true

        notificationsRef.push().setValue(notificationMap)
    }

}