package com.example.instagramcloneapp.Adapters

import android.content.Context
import android.provider.Settings.System.getString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Fragments.ProfileFragment
import com.example.instagramcloneapp.Model.User
import com.example.instagramcloneapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.user_item_layout.view.*

class UserAdapter (private var mContext: Context,
                   private var mUser: List<User>,
                   private  var isFragment: Boolean = false) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    private var currentUser: FirebaseUser? = FirebaseAuth.getInstance().currentUser

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.user_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = mUser[position]
        holder.usernameTextView.text = user.username
        holder.fullnameTextView.text = user.fullName
        Picasso.get().load(user.profilePic).resize(50, 50).placeholder(R.drawable.profile).into(holder.userProfilePic)

        holder.followBtn.visibility = View.VISIBLE

        when (user.uid) {
            currentUser?.uid -> holder.followBtn.visibility = View.GONE
            else -> checkFollowingStatus(user.uid, holder.followBtn)
        }

        holder.itemView.setOnClickListener(View.OnClickListener {
            if (isFragment) {
                val pref = mContext.getSharedPreferences("PREFS", Context.MODE_PRIVATE).edit()
                pref.putString("profileId", user.uid)
                pref.apply()

                (mContext as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, ProfileFragment()).commit()
            }

        })

        holder.followBtn.setOnClickListener{
            if (holder.followBtn.text.toString() == "Follow") {
                // following a person
                currentUser?.uid.let{it1->
                    FirebaseDatabase.getInstance().reference
                        .child("Follow").child(it1.toString())
                        .child("Following").child(user.uid)
                        .setValue(true).addOnCompleteListener{task ->
                            if (task.isSuccessful) {
                                // adding user to persons followers list
                                currentUser?.uid.let{it2->
                                    FirebaseDatabase.getInstance().reference
                                        .child("Follow").child(user.uid)
                                        .child("Followers").child(it2.toString())
                                        .setValue(true).addOnCompleteListener {task2 ->
                                            if (task2.isSuccessful) {

                                            }
                                        }
                                    }
                            }
                        }
                }
                sendNotification(user.uid)
            } else if (holder.followBtn.text.toString() == "Following") {
                // unfollowing a person
                currentUser?.uid.let{it1->
                    FirebaseDatabase.getInstance().reference
                        .child("Follow").child(it1.toString())
                        .child("Following").child(user.uid)
                        .removeValue().addOnCompleteListener{task ->
                            if (task.isSuccessful) {
                                // removing user from persons followers list
                                currentUser?.uid.let{it2->
                                    FirebaseDatabase.getInstance().reference
                                        .child("Follow").child(user.uid)
                                        .child("Followers").child(it2.toString())
                                        .removeValue().addOnCompleteListener {task2 ->
                                            if (task2.isSuccessful) {

                                            }
                                        }
                                }
                            }
                        }
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return mUser.size
    }

    class ViewHolder (@NonNull itemView: View): RecyclerView.ViewHolder(itemView) {
        var usernameTextView: TextView = itemView.findViewById(R.id.search_username)
        var fullnameTextView: TextView = itemView.findViewById(R.id.search_fullName)
        var userProfilePic: CircleImageView = itemView.findViewById(R.id.search_picture)
        var followBtn: Button = itemView.findViewById(R.id.search_follow_btn)
    }

    private fun checkFollowingStatus(userID: String, followButton: Button) {
        val followingRef = currentUser?.uid.let { it1 ->
            FirebaseDatabase.getInstance().reference
                .child("Follow").child(it1.toString())
                .child("Following")
        }
        followingRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.child(userID).exists()) {
                    followButton.text = "Following"
                } else {
                    followButton.text = "Follow"
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun sendNotification(userId: String) {
        val notificationsRef = FirebaseDatabase.getInstance().reference
            .child("Notifications").child(userId)

        val notificationMap = HashMap<String, Any>()
        notificationMap["userId"] = currentUser!!.uid
        notificationMap["text"] = "started following you."
        notificationMap["postId"] = ""
        notificationMap["isPost"] = false

        notificationsRef.push().setValue(notificationMap)
    }

}



