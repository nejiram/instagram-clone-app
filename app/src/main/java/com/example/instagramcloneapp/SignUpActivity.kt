package com.example.instagramcloneapp

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.util.*
import kotlin.collections.HashMap

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        signin_link_btn.setOnClickListener{
            startActivity(Intent(this, SignInActivity::class.java))
        }

        sign_up_btn.setOnClickListener {
            createAccount()
        }

    }

    private fun createAccount() {
        val email = signup_email.text.toString()
        val fullName = signup_full_name.text.toString()
        val username = signup_username.text.toString()
        val password = signup_password.text.toString()

        when {
            TextUtils.isEmpty(email) -> Toast.makeText(this, "Email is required!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(fullName) -> Toast.makeText(this, "Full name is required!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(username) -> Toast.makeText(this, "Username is required!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(password) -> Toast.makeText(this, "Password is required!", Toast.LENGTH_LONG).show()

            else -> {

                sign_up_progress_bar.visibility = View.VISIBLE

                val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
                mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener{task ->
                        if (task.isSuccessful) {
                            saveUserInfo(email, fullName, username)
                        } else {
                            val message = task.exception.toString()
                            Toast.makeText(this, "Error: $message", Toast.LENGTH_LONG).show()
                            mAuth.signOut()
                            sign_up_progress_bar.visibility = View.GONE
                        }
                    }
            }

        }
    }

    private fun saveUserInfo(email: String, fullName: String, username: String) {
        val currentUserID = FirebaseAuth.getInstance().currentUser!!.uid
        val usersRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("Users")
        val userMap = HashMap<String, Any> ()

        userMap["uid"] = currentUserID
        userMap["email"] = email
        userMap["fullName"] = fullName.toLowerCase(Locale.ROOT)
        userMap["username"] = username.toLowerCase(Locale.ROOT)
        userMap["bio"] = "Hey, I am $fullName, and I am new to Instagram Clone App!"
        userMap["profilePic"] = "https://firebasestorage.googleapis.com/v0/b/instagram-clone-app-f9b87.appspot.com/o/" +
                "Default%20Images%2Fprofile.png?alt=media&token=403969cb-8519-4ea7-bb32-fc357f99ec23"

        usersRef.child(currentUserID).setValue(userMap).addOnCompleteListener{task ->
            if (task.isSuccessful) {

                sign_up_progress_bar.visibility = View.GONE
                Toast.makeText(this, "Account created successfully!", Toast.LENGTH_LONG).show()

                FirebaseDatabase.getInstance().reference
                        .child("Follow").child(currentUserID)
                        .child("Following").child(currentUserID)
                        .setValue(true)

                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            } else {
                val message = task.exception.toString()
                Toast.makeText(this, "Error: $message", Toast.LENGTH_LONG).show()
                FirebaseAuth.getInstance().signOut()
                sign_up_progress_bar.visibility = View.GONE
            }
        }
    }

}