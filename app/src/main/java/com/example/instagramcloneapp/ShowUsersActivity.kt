package com.example.instagramcloneapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramcloneapp.Adapters.CommentAdapter
import com.example.instagramcloneapp.Adapters.UserAdapter
import com.example.instagramcloneapp.Model.Comment
import com.example.instagramcloneapp.Model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_show_users.*
import kotlinx.android.synthetic.main.fragment_search.view.*

class ShowUsersActivity : AppCompatActivity() {

    private var currentUser: FirebaseUser?= null

    var id: String = ""
    var title: String = ""

    var userAdapter: UserAdapter ?= null
    var userList: MutableList<User> ?= null
    var userIdList: MutableList<String> ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_users)

        currentUser = FirebaseAuth.getInstance().currentUser

        id = intent.getStringExtra("id").toString()
        title = intent.getStringExtra("title").toString()

        show_users_title.text = title

        show_users_back_btn.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        var recyclerView: RecyclerView
        recyclerView = findViewById(R.id.show_users_recyclerview)
        recyclerView.setHasFixedSize(true)
        var linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        userList = ArrayList()
        userAdapter = UserAdapter(this, userList as ArrayList<User>, false)
        recyclerView.adapter = userAdapter

        userIdList = ArrayList()

        when (title) {
            "Likes" -> getLikes()
            "Followers" -> getFollowers()
            "Following" -> getFollowing()
        }

    }

    private fun getLikes() {
        val likesRef = FirebaseDatabase.getInstance().reference.child("Likes").child(id!!)

        likesRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    (userIdList as ArrayList<String>).clear()
                    for (snapshot in datasnapshot.children) {
                        (userIdList as ArrayList<String>).add(snapshot.key!!)
                    }
                    showUsers()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun getFollowers() {
        val followersRef = FirebaseDatabase.getInstance().reference
            .child("Follow").child(id!!).child("Followers")
        followersRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    (userIdList as ArrayList<String>).clear()
                    for (snapshot in datasnapshot.children) {
                        (userIdList as ArrayList<String>).add(snapshot.key!!)
                    }
                    showUsers()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun getFollowing() {
        val followingRef = FirebaseDatabase.getInstance().reference
            .child("Follow").child(id!!).child("Following")
        followingRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    (userIdList as ArrayList<String>).clear()
                    for (snapshot in datasnapshot.children) {
                        if (snapshot.key!! != id){
                            (userIdList as ArrayList<String>).add(snapshot.key!!)
                        }
                    }
                    showUsers()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun showUsers() {
        val usersRef = FirebaseDatabase.getInstance().reference.child("Users")
        usersRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.exists()) {
                    (userList as ArrayList<User>).clear()
                    for (snapshot in datasnapshot.children) {
                        val user = snapshot.getValue(User::class.java)
                        for (id in userIdList!!) {
                            if (user!!.uid == id) {
                                if (title == "Following" && id == currentUser!!.uid) {
                                    continue
                                } else {
                                    (userList as ArrayList<User>).add(user)
                                }
                            }
                        }
                        userAdapter?.notifyDataSetChanged()
                    }
                }

            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

}